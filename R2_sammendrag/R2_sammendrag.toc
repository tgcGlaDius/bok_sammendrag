\contentsline {section}{\numberline {1}Trigonometri}{4}{section.1}
\contentsline {subsection}{\numberline {1.1}Trigonometri i rettvinklete trekanter}{5}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}enhetssirkelen}{6}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}symetrier p\IeC {\r a} enhetssirkelen}{6}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Trigonometriske grunnlikninger}{6}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}Enhetsformelen}{6}{subsection.1.5}
\contentsline {subsection}{\numberline {1.6}Sum og differanse}{6}{subsection.1.6}
\contentsline {subsection}{\numberline {1.7}Dobbel vinkel}{6}{subsection.1.7}
\contentsline {section}{\numberline {2}Vektorer og geometri i rommet}{7}{section.2}
\contentsline {subsection}{\numberline {2.1}Pyramider}{9}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Vektorkoordinater i rommet}{9}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Skalarproduktet}{9}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}parameterframstilling for rette linjer}{9}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}vektorprodukt}{9}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}likningsframstilling for plan}{10}{subsection.2.6}
\contentsline {subsection}{\numberline {2.7}avstand punkt til plan}{10}{subsection.2.7}
\contentsline {subsection}{\numberline {2.8}avstand mellom punkt og linje}{10}{subsection.2.8}
\contentsline {subsection}{\numberline {2.9}hvordan ligger planene i rommet}{10}{subsection.2.9}
\contentsline {subsection}{\numberline {2.10}parameterframstilling for plan}{10}{subsection.2.10}
\contentsline {subsection}{\numberline {2.11}areal og volum med vektorprodukt}{11}{subsection.2.11}
\contentsline {subsection}{\numberline {2.12}likningsframstilling for kuleflate}{11}{subsection.2.12}
\contentsline {subsection}{\numberline {2.13}parameterframstilling for kuleflater}{11}{subsection.2.13}
\contentsline {subsection}{\numberline {2.14}paralellitet og vektorer i samme plan}{11}{subsection.2.14}
\contentsline {section}{\numberline {3}Trigonometriske funksjoner}{12}{section.3}
\contentsline {subsection}{\numberline {3.1}Radianer}{12}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Trigonometriske likninger}{13}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Grafene til sin x og cos x}{13}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Harmoniske svingninger}{13}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Omskrivning av sinus}{13}{subsection.3.5}
\contentsline {subsection}{\numberline {3.6}L\IeC {\o }sning av likningen a sin cx + b cos cx = d}{14}{subsection.3.6}
\contentsline {subsection}{\numberline {3.7}Modellering av periodiske funksjoner}{14}{subsection.3.7}
\contentsline {subsection}{\numberline {3.8}Derivasjon av sinus}{14}{subsection.3.8}
\contentsline {subsection}{\numberline {3.9}Derivasjon av cosinus og tangens}{14}{subsection.3.9}
\contentsline {subsection}{\numberline {3.10}Dr\IeC {\o }fting av sammensatte trigonometriske funksjoner}{14}{subsection.3.10}
\contentsline {subsection}{\numberline {3.11}modellering p\IeC {\r a} digitalt verkt\IeC {\o }y}{15}{subsection.3.11}
\contentsline {section}{\numberline {4}Integrasjon}{16}{section.4}
\contentsline {subsection}{\numberline {4.1}Bestemt integral}{17}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Utregning av bestemt integral}{17}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Ubestemt integral}{18}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Arealberegninger}{18}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Regler for bestemte integraler. Arealet mellom to grafer}{18}{subsection.4.5}
\contentsline {subsection}{\numberline {4.6}Samlet mengde. Gjennomsnitt}{18}{subsection.4.6}
\contentsline {subsection}{\numberline {4.7}Volum av omdreiningslegemer}{18}{subsection.4.7}
\contentsline {subsection}{\numberline {4.8}Integrasjon ved substitusjon}{19}{subsection.4.8}
\contentsline {subsection}{\numberline {4.9}Delvis integrasjon}{19}{subsection.4.9}
\contentsline {subsection}{\numberline {4.10}Integrasjon ved delbr\IeC {\o }koppspalting}{19}{subsection.4.10}
\contentsline {subsection}{\numberline {4.11}Om \IeC {\r a} velge riktig metode}{19}{subsection.4.11}
\contentsline {section}{\numberline {5}Differensiallikninger av f\IeC {\o }rste orden}{20}{section.5}
\contentsline {subsection}{\numberline {5.1}Differensiallikninger og l\IeC {\o }sningskurver}{20}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Eksakte differensiallikninger av f\IeC {\o }rste orden}{20}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}differensiallikningen $y'+ay=b$}{21}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}Modeller og bruksomr\IeC {\r a}der for differensiallikningen $y'+ay=b$}{21}{subsection.5.4}
\contentsline {subsection}{\numberline {5.5}differensiallikninger i fysikk. Newtons andre lov}{21}{subsection.5.5}
\contentsline {subsection}{\numberline {5.6}line\IeC {\ae }re differensiallikninger av f\IeC {\o }rste orden}{21}{subsection.5.6}
\contentsline {subsection}{\numberline {5.7}separable differensiallikninger}{21}{subsection.5.7}
\contentsline {subsection}{\numberline {5.8}Differensiallikninger i biologi og medisin}{22}{subsection.5.8}
\contentsline {subsection}{\numberline {5.9}B\IeC {\ae }reevne og logistisk vekst}{22}{subsection.5.9}
\contentsline {subsection}{\numberline {5.10}Retningsdiagrammer. numerisk l\IeC {\o }sning}{22}{subsection.5.10}
\contentsline {section}{\numberline {6}F\IeC {\o }lger og rekker}{23}{section.6}
\contentsline {subsection}{\numberline {6.1}F\IeC {\o }lger og rekker}{23}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Aritmetiske f\IeC {\o }lger og rekker}{24}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Geometriske f\IeC {\o }lger og rekker}{24}{subsection.6.3}
\contentsline {subsection}{\numberline {6.4}Sparing og l\IeC {\r a}n}{24}{subsection.6.4}
\contentsline {subsection}{\numberline {6.5}Konvergente Geometriske rekker}{24}{subsection.6.5}
\contentsline {subsection}{\numberline {6.6}Konvergensomr\IeC {\r a}det for geometriske rekker}{25}{subsection.6.6}
\contentsline {subsection}{\numberline {6.7}Matematisk induksjon}{25}{subsection.6.7}
\contentsline {subsection}{\numberline {6.8}Rekursive tallf\IeC {\o }lger}{25}{subsection.6.8}
\contentsline {section}{\numberline {7}Differensiallikninger av andre orden}{26}{section.7}
\contentsline {subsection}{\numberline {7.1}Differensiallikninger av andre orden}{26}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Differensiallikningen $y''-k^2y=0$}{26}{subsection.7.2}
\contentsline {subsection}{\numberline {7.3}Differensiallikningen $y''+k^2y=0$}{26}{subsection.7.3}
\contentsline {subsection}{\numberline {7.4}Frie svingninger uten demping}{26}{subsection.7.4}
\contentsline {subsection}{\numberline {7.5}vi l\IeC {\o }ser likningen $y''+by'+cy=0$}{26}{subsection.7.5}
\contentsline {subsection}{\numberline {7.6}l\IeC {\o }sninger til likningen $y''+by'+cy=0$ med karakteristisk likning}{27}{subsection.7.6}
\contentsline {subsection}{\numberline {7.7}Frie svingninger med demping}{27}{subsection.7.7}
\contentsline {subsection}{\numberline {7.8}Differensiallikningen $y''+P(x)*y'+Q(x)*y=0$}{27}{subsection.7.8}
\contentsline {section}{\numberline {8}tips for Del 2}{28}{section.8}
\contentsline {subsection}{\numberline {8.1}fine komandoer i CAS og geogebra}{28}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}generelle tips}{28}{subsection.8.2}
